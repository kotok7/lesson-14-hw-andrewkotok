package MyHashSetHW;

import java.util.*;

public class MyHashSet<T> implements Set<T> {

    private static final int DEFAULT_CAPACITY = 16;

    public static class Node<T> {
        Object value;
        Node next;

        public Node(Object value) {
            this.value = value;
        }

        boolean hasValue(Object value) {
            return this.value.equals(value);
        }
    }

    private Node[] nodes;
    private int size = 0;
    private int capacity;

    public MyHashSet() {
        this(DEFAULT_CAPACITY);
    }

    public MyHashSet(int capacity) {
        this.capacity = capacity;
        this.nodes = new Node[capacity];
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        MyHashSet<T> tmp = new MyHashSet<>();
        boolean flag = false;
        for (Object e : c){
            if (contains(e)){
                tmp.add((T) e);
                flag = true;
            }
        }
        this.clear();
        this.addAll(tmp);
        return flag;
    }
// retainAll Done

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean flag = false;
        for (Object e : c){
            if (remove(e)){
                flag = true;
            }
        }
        return flag;
    }
    // removeAll Done

    @Override
    public boolean addAll(Collection<? extends T> c) {
        boolean flag= false;
        for (T e : c){
            if (add(e)){
                flag = true;
            }
        }
        return flag;
    }
// addAll Done

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        if (o == null) return false;
        int index = getIndex(o);
        for (Node cur = nodes[index]; cur != null; cur = cur.next) {
            if (cur.hasValue(o)) return true;
        }
        return false;
    }

    @Override
    public boolean add(T value) {
        if (value == null) return false;
        Node node = new Node(value);
        int index = getIndex(value);
        if (nodes[index]==null){
            nodes[index] = node;
            size++;
            return true;
        }
        for (Node cur = nodes[index]; cur != null; cur = cur.next) {
            if (cur.hasValue(value)) {
                cur.value = value;
                return false;
            } else if (cur.next == null) {
                cur.next = node;
                size++;
                break;
            }
        }
        return true;
    }

    private int getIndex(Object o) {
        return Math.abs(o.hashCode() % nodes.length);
    }


    @Override
    public Iterator<T> iterator() {
        return new MyHashSetIterator();
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        T1[] result = Arrays.copyOf(a, size);
        int i = 0;
        for (T elem : this) {
            result[i++] = (T1) elem;
        }
        return result;
    }

    @Override
    public boolean remove(Object o) {
        int index = getIndex(o);
        if (nodes[index] == null) {
            return false;
        }
        Node prev = null;
        for (Node cur = nodes[index]; cur != null; prev = cur, cur = cur.next) {
            if (cur.hasValue(o)) {
                size--;
                if (cur == nodes[index]) {
                    nodes[index] = cur.next;
                }else {
                    prev.next = cur.next;
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!this.contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void clear() {
        this.nodes = new Node[capacity];
    }

    private class MyHashSetIterator implements Iterator<T>{
        int index =-1;
        int count = 0;
        Node cur = null;

        @Override
        public boolean hasNext() {
            return count<size;
        }

        private void findNext() {
            if(cur!=null && cur.next!=null){
                cur = cur.next;
            } else {
                do {
                    cur = nodes[++index];
                } while (cur ==null && index< nodes.length-1);
            }

        }

        @Override
        public T next() {
            if(!hasNext()) throw  new NoSuchElementException();
            findNext();
            count++;
            return (T) cur.value;
        }
    }
}