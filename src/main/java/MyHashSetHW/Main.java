package MyHashSetHW;

import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<String> mySet = new MyHashSet<>();
        Set<String> mySet2 = new MyHashSet<>();
        Set<String> mySet3 = new MyHashSet<>();


        mySet.add("A");
        mySet.add("B");
        mySet.add("A");
        mySet.add("C");
        mySet.add("B");
        mySet.add("D");
        for (String s : mySet) {
            System.out.printf("%s; ", s);
        }
        System.out.println("\n");
        mySet2.add("A2");
        mySet2.add("B2");

        mySet.addAll(mySet2);
        for (String s : mySet) {
            System.out.printf("%s; ", s);
        }
        System.out.println("\n");

        mySet.removeAll(mySet2);
        for (String s : mySet) {
            System.out.printf("%s; ", s);
        }
        System.out.println("\n");

        mySet3.add("B");
        mySet3.add("C");
        mySet.retainAll(mySet3);
        for (String s : mySet) {
            System.out.printf("%s; ", s);
        }


    }
}
